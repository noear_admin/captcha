package com.anji.captcha.demo;

import org.noear.solon.Solon;
import org.noear.solon.web.cors.CrossFilter;


/**
 * @author noear 2021/9/2 created
 */
public class App {
    public static void main(String[] args) {
        Solon.start(App.class, args, app->{
            app.filter(new CrossFilter());
        });
    }
}
